import time

from utils import *
from natsort import natsort_key
from multiprocessing.pool import ThreadPool


# PARAMS CHANGE
CODEX_IMG_TYPE = 'BCTC3_CDKT_A'
CODEX_RC_TYPE = '2'
TABLE_TYPE_INDEX = 0
CODEX_TABLE_TYPE = 'BCTC3_CDKT_A_2'

CSV_FOLDER_NAME = 'VATI_CSV' + '/' + CODEX_TABLE_TYPE
FOLDER_SAVE_IMAGE_NAME = 'VATI_IMG_BCTC' + '/' + CODEX_TABLE_TYPE

SIZE_HEADER_FONT = 24
SIZE_ROW_FONT = 24

# HEIGHT_LINES = 28

PADDING_TABLE_X = [12, 196]
PADDING_TABLE_Y = [12, 196]

BORDERLESS = True
WIDTH_BORDER_LINE = 1

RANGE_CHANGE_WIDTH_COLUMN = [-10, 25]
RANGE_CHANGE_HEIGHT_ROW = [0, 25]

# RANGE_CHANGE_WIDTH_COLUMN = [0, 0]
# RANGE_CHANGE_HEIGHT_ROW = [0, 0]

# PARAMS FIXED
FONT_NAME = ''
BOLD_FONT_NAME = 'fonts\\timesbd.ttf'
NORMAL_FONT_NAME = 'fonts\\times.ttf'
ITALIC_FONT_NAME = 'fonts\\arialic.ttf'

# PARAMS FIXED
BASE_ADDR = os.getcwd()
CSV_FOLDER_DIR = join_path(BASE_ADDR, CSV_FOLDER_NAME)
HEADER_FONT = ImageFont.truetype(join_path(BASE_ADDR, BOLD_FONT_NAME), size=SIZE_HEADER_FONT)
ROW_FONT = ImageFont.truetype(join_path(BASE_ADDR, NORMAL_FONT_NAME), size=SIZE_ROW_FONT)
FOLDER_SAVE_IMAGE_DIR = join_path(BASE_ADDR, FOLDER_SAVE_IMAGE_NAME)
HEIGHT_FONT = get_height_of_some_tokens('PROJECT HOON @131220', HEADER_FONT)
HEIGHT_LINES = HEIGHT_FONT + 3

S_R_C = RANGE_CHANGE_WIDTH_COLUMN[0]
E_R_C = RANGE_CHANGE_WIDTH_COLUMN[1]
S_R_R = RANGE_CHANGE_HEIGHT_ROW[0]
E_R_R = RANGE_CHANGE_HEIGHT_ROW[1]

NUMS_PROCESSES = 8
NUMS_SUB_PROCESSES = 4


def identify_table(data):
    first_row = data[0]
    nums_rows = len(data)
    nums_columns = len(first_row)

    return nums_rows, nums_columns, CODEX_TABLE_TYPE

def calc_table_width(nums_columns, COLUMN_WIDTH_RANDOM, TOTAL_MARGIN_X):
    table_width = 0
    WoTIC = []
    for i in range(nums_columns):
        COLUMN_WIDTH_RANDOM[i] = round((1 + randint_differrence_range(S_R_C, E_R_C) / 100) * COLUMN_WIDTH_RANDOM[i])
        column_width = COLUMN_WIDTH_RANDOM[i]
        if column_width < 80:
            mean_width = column_width - round(0.1 * TOTAL_MARGIN_X)
        else:    
            mean_width = column_width - TOTAL_MARGIN_X
        WoTIC.append(mean_width)
        table_width += column_width
    return table_width, WoTIC, COLUMN_WIDTH_RANDOM

def calc_table_height(data, TABLE_TYPE, WoTIC, CONST_HEIGHT, TOTAL_MARGIN_Y_EXTEND):
    table_height = 0
    height_per_row = []

    if TABLE_TYPE[0]:
        row_idx = -1
        for row in data:
            row_idx += 1
            col_idx = -1
            max_current_row_height = 0

            ROW_FONT_CUR = HEADER_FONT if row_idx in TABLE_TYPE[13] else ROW_FONT
            if row_idx > 0:
                for item in row:
                    col_idx += 1
                    current_width = get_width_of_some_tokens(item, ROW_FONT_CUR)
                    fixed_width = WoTIC[col_idx]
                    nums_lines = (current_width + fixed_width - 1) // fixed_width
                    cell_height = nums_lines * CONST_HEIGHT + TOTAL_MARGIN_Y_EXTEND
                    if cell_height > max_current_row_height:
                        max_current_row_height = cell_height
            else:
                for item in TABLE_TYPE[4]:
                    nums_lines = len(item)
                    cell_height = nums_lines * CONST_HEIGHT + TOTAL_MARGIN_Y_EXTEND
                    if cell_height > max_current_row_height:
                        max_current_row_height = cell_height

            max_height = round(max_current_row_height * (1 + randint_differrence_range(S_R_R, E_R_R) / 100))
            height_per_row.append(max_height)
            table_height += max_height
            if row_idx in TABLE_TYPE[14]:
                table_height += TABLE_TYPE[15]
    else:
        row_idx = -1
        for row in data:
            row_idx += 1
            col_idx = -1
            max_current_row_height = 0

            ROW_FONT_CUR = HEADER_FONT if row_idx in TABLE_TYPE[13] else ROW_FONT
            for item in row:
                col_idx += 1
                current_width = get_width_of_some_tokens(item, ROW_FONT_CUR)
                fixed_width = WoTIC[col_idx]
                nums_lines = (current_width + fixed_width - 1) // fixed_width
                cell_height = nums_lines * CONST_HEIGHT + TOTAL_MARGIN_Y_EXTEND
                if cell_height > max_current_row_height:
                    max_current_row_height = cell_height

            max_height = round(max_current_row_height * (1 + randint_differrence_range(S_R_R, E_R_R) / 100))
            height_per_row.append(max_height)
            table_height += max_height
            if row_idx in TABLE_TYPE[14]:
                table_height += TABLE_TYPE[15]

    return table_height, height_per_row

def table_structure_range(data, txt_dir):
    nums_rows, nums_columns, codex_table_type = identify_table(data)
    TABLE_TYPE = DICT_TABLE_TYPE[codex_table_type].copy()

    with open(txt_dir, 'r') as txt_file:
        txt_data = txt_file.readlines()
        arr_txt_data = [eval(i) for i in txt_data]
        TABLE_TYPE[13] = arr_txt_data[0].copy()
        TABLE_TYPE[14] = arr_txt_data[1].copy()
        TABLE_TYPE[17] = arr_txt_data[2].copy()
        additional_spc = arr_txt_data[3].copy()
        ad_content_spc = arr_txt_data[4].copy()
        ad_alignment_spc = arr_txt_data[5].copy()

        if len(additional_spc) > 0:
            TABLE_TYPE[10] = TABLE_TYPE[10].copy() + additional_spc
        # if len(ad_content_spc) > 0:
            TABLE_TYPE[11] = TABLE_TYPE[11].copy() + ad_content_spc
        # if len(ad_alignment_spc) > 0:
            TABLE_TYPE[12] = TABLE_TYPE[12].copy() + ad_alignment_spc

    TOTAL_MARGIN_X = TABLE_TYPE[8][0] + TABLE_TYPE[8][2]
    TOTAL_MARGIN_Y = TABLE_TYPE[8][1] + TABLE_TYPE[8][3]
    TOTAL_MARGIN_Y_EXTEND = TOTAL_MARGIN_Y - TABLE_TYPE[16]
    CONST_HEIGHT = HEIGHT_LINES + TABLE_TYPE[16]

    COLUMN_WIDTH_INIT = TABLE_TYPE[9].copy()

    # WoTIC - Width of Text In Column
    table_width, WoTIC, COLUMN_WIDTH_RANDOM = calc_table_width(nums_columns, COLUMN_WIDTH_INIT, TOTAL_MARGIN_X)
    table_height, height_per_row = calc_table_height(data, TABLE_TYPE, WoTIC, CONST_HEIGHT, TOTAL_MARGIN_Y_EXTEND)
    
    return nums_rows, nums_columns, table_height, table_width, height_per_row, WoTIC, TABLE_TYPE, COLUMN_WIDTH_RANDOM

def draw_row(PACKET_CONSTANT, row_idx, row, WoTIC, height_per_row, borderless, draw, height_line, height_font):
    START_POSITION_X, TABLE_TYPE, COLUMN_WIDTH_RANDOM, FLAT_TABLE_TYPE_10, POS_REC_Y, START_CELL_INDEX = PACKET_CONSTANT
    cur_pos_rec_y = POS_REC_Y[row_idx]
    cell_idx = START_CELL_INDEX[row_idx]
    cur_pos_rec_x = START_POSITION_X

    if row_idx in TABLE_TYPE[13] or row_idx < TABLE_TYPE[1]:
        ROW_FONT_CUR = HEADER_FONT
    else:
        ROW_FONT_CUR = ROW_FONT

    # Draw row per cell (by column)
    col_idx = -1
    for item in row:
        col_idx += 1
        cell_idx += 1

        mean_text_column_width = WoTIC[col_idx]
        cell_width = COLUMN_WIDTH_RANDOM[col_idx]
        cell_height = height_per_row[row_idx]

        # Check cell is not in spanning cell, draw cell
        if cell_idx not in FLAT_TABLE_TYPE_10:
            if not borderless:
                xy_coordinates_cell = (cur_pos_rec_x, cur_pos_rec_y, cur_pos_rec_x + cell_width, cur_pos_rec_y + cell_height)
                draw.rectangle(xy_coordinates_cell, outline='black', width=WIDTH_BORDER_LINE)
            
            if row_idx == 0 and TABLE_TYPE[0]:
                if col_idx < TABLE_TYPE[2]:
                    relative_index = col_idx
                else:
                    relative_index = TABLE_TYPE[2] - 1
                lines = TABLE_TYPE[4][relative_index]
            else: 
                lines = my_textwrap_tokens(item, mean_text_column_width, ROW_FONT_CUR)   

            # Alignment vertical row
            cur_pos_txt_y = cur_pos_rec_y
            if TABLE_TYPE[0]:
                if row_idx > 0:   
                    vertical_string_row = TABLE_TYPE[7][col_idx][1]
                    margin_space_row_y = get_margin_space_y(vertical_string_row, cell_height, len(lines), height_line, height_font, TABLE_TYPE[8], TABLE_TYPE[16])
                    cur_pos_txt_y += margin_space_row_y
                
                elif row_idx == 0:
                    vertical_string_row_header = TABLE_TYPE[5][relative_index][1]
                    margin_space_row_header_y = get_margin_space_y(vertical_string_row_header, cell_height, len(lines), height_line, height_font, TABLE_TYPE[8], TABLE_TYPE[16])
                    cur_pos_txt_y += margin_space_row_header_y
            
            else:
                vertical_string_row = TABLE_TYPE[7][col_idx][1]
                margin_space_row_y = get_margin_space_y(vertical_string_row, cell_height, len(lines), height_line, height_font, TABLE_TYPE[8], TABLE_TYPE[16])
                cur_pos_txt_y += margin_space_row_y

            # Draw cell per line
            for line in lines:
                # Alignment horizontal row
                cur_pos_txt_x = cur_pos_rec_x
                if TABLE_TYPE[0]:
                    if row_idx > 0:
                        horizontal_string_row = TABLE_TYPE[7][col_idx][0]
                        margin_space_row_x = get_margin_space_x(horizontal_string_row, cell_width, line, ROW_FONT_CUR, TABLE_TYPE[8])
                        cur_pos_txt_x += margin_space_row_x
                    
                    elif row_idx == 0:
                        horizontal_string_row_header = TABLE_TYPE[5][relative_index][0]
                        margin_space_row_header_x = get_margin_space_x(horizontal_string_row_header, cell_width, line, HEADER_FONT, TABLE_TYPE[8])
                        cur_pos_txt_x += margin_space_row_header_x
                
                else:
                    horizontal_string_row = TABLE_TYPE[7][col_idx][0]
                    margin_space_row_x = get_margin_space_x(horizontal_string_row, cell_width, line, ROW_FONT_CUR, TABLE_TYPE[8])
                    cur_pos_txt_x += margin_space_row_x

                # Determine the coordinates of the line content, draw the line content
                width_line = get_width_of_some_tokens(line, ROW_FONT_CUR)
                xy_coordinates_text = (cur_pos_txt_x, cur_pos_txt_y, cur_pos_txt_x + width_line, cur_pos_txt_y + height_line)
                draw.text(xy_coordinates_text, line, font=ROW_FONT_CUR, fill='black')
                
                # Increase y_coordinate of text to next line position
                cur_pos_txt_y += height_line

        # Increase x_coordinate of cell to next cell
        cur_pos_rec_x += cell_width

def draw_table_per_row(data, PACKET_CONSTANT, WoTIC, height_per_row, borderless, draw, height_line, height_font):
    row_idx = -1
    for row in data:
        row_idx += 1
        draw_row(PACKET_CONSTANT, row_idx, row, WoTIC, height_per_row, borderless, draw, height_line, height_font)

def draw_spanning_cell(PACKET_CONSTANT_DTPR, nums_columns, height_per_row, borderless, draw, height_line, height_font):
    START_POSITION_X, START_POSITION_Y, COLUMN_WIDTH_RANDOM, TABLE_TYPE = PACKET_CONSTANT_DTPR

    for i in range(len(TABLE_TYPE[10])):
        start_cell = TABLE_TYPE[10][i][0]
        end_cell = TABLE_TYPE[10][i][-1]
        
        spc_width, spc_height, start_coordinates_spc_x, start_coordinates_spc_y = size_of_merged_cell(
            start_cell, end_cell, nums_columns, height_per_row, COLUMN_WIDTH_RANDOM, START_POSITION_X, START_POSITION_Y, TABLE_TYPE[14], TABLE_TYPE[15])

        if not borderless:
            xy_coordinates_spc = (start_coordinates_spc_x, start_coordinates_spc_y, start_coordinates_spc_x + spc_width, start_coordinates_spc_y + spc_height)
            draw.rectangle(xy_coordinates_spc, outline='black', width=WIDTH_BORDER_LINE)

        lines = TABLE_TYPE[11][i]
        
        _, _, row_end_spc, _ = identify_merged_cell(start_cell, end_cell, nums_columns)
        if row_end_spc in TABLE_TYPE[13] or row_end_spc < TABLE_TYPE[1]:
            ROW_FONT_CUR = HEADER_FONT
        else:
            ROW_FONT_CUR = ROW_FONT
        
        # Alignment vertical spc
        spc_pos_txt_y = start_coordinates_spc_y
        vertical_string_spc = TABLE_TYPE[12][i][1]
        margin_space_spc_y = get_margin_space_y(vertical_string_spc, spc_height, len(lines), height_line, height_font, TABLE_TYPE[8], TABLE_TYPE[16])
        spc_pos_txt_y += margin_space_spc_y

        for line in lines:
            # Alignment horizontal spc
            spc_pos_txt_x = start_coordinates_spc_x
            horizontal_string_spc = TABLE_TYPE[12][i][0]
            margin_space_spc_x = get_margin_space_x(horizontal_string_spc, spc_width, line, HEADER_FONT, TABLE_TYPE[8])
            spc_pos_txt_x += margin_space_spc_x

            # Determine the coordinates of the line content, draw the line content
            width_line = get_width_of_some_tokens(line, ROW_FONT_CUR)
            xy_coordinates_spc_text = (spc_pos_txt_x, spc_pos_txt_y, spc_pos_txt_x + width_line, spc_pos_txt_y + height_line)
            draw.text(xy_coordinates_spc_text, line, font=ROW_FONT_CUR, fill='black')
            
            # Increase y_coordinate of text to next line position
            spc_pos_txt_y += height_line

def draw_table_line(PACKET_CONSTANT_DTL, table_width, height_per_row, draw, nums_rows):
    # TODO: Draw line
    # FIXME: Change the code below according to the type of gen table

    START_POSITION_X, START_POSITION_Y, COLUMN_WIDTH_RANDOM, TABLE_TYPE = PACKET_CONSTANT_DTL

    # Draw line in header
    start_x_line_header = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] - COLUMN_WIDTH_RANDOM[-2]
    start_y_line_header = START_POSITION_Y + height_per_row[0]
    end_x_line_header = START_POSITION_X + table_width
    end_y_line_header = start_y_line_header
    xy_coordinates_line_header = (start_x_line_header, start_y_line_header, end_x_line_header, end_y_line_header)
    draw_line(draw, xy_coordinates_line_header, 2)

    start_x_line_row = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] - COLUMN_WIDTH_RANDOM[-2] + 10
    start_y_line_row = START_POSITION_Y
    end_x_line_row = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] - 5
    end_y_line_row = 0
    for idx in range(nums_rows):
        if idx < TABLE_TYPE[14][-1]:
            start_y_line_row += height_per_row[idx]
            if idx in TABLE_TYPE[14]:
                start_y_line_row += TABLE_TYPE[15]
        else:
            start_y_line_row -= TABLE_TYPE[15] / 2
            end_y_line_row = start_y_line_row
            break
    xy_coordinates_line_row = (start_x_line_row, start_y_line_row, end_x_line_row, end_y_line_row)
    draw_line(draw, xy_coordinates_line_row, 2)

    start_x_line_row = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] + 10
    end_x_line_row = START_POSITION_X + table_width - 5
    xy_coordinates_line_row = (start_x_line_row, start_y_line_row, end_x_line_row, end_y_line_row)
    draw_line(draw, xy_coordinates_line_row, 2)

    start_x_line_row = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] - COLUMN_WIDTH_RANDOM[-2] + 10
    start_y_line_row += height_per_row[-1] + TABLE_TYPE[15]
    end_x_line_row = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] - 5
    end_y_line_row = start_y_line_row
    xy_coordinates_line_row = (start_x_line_row, start_y_line_row, end_x_line_row, end_y_line_row)
    draw_line(draw, xy_coordinates_line_row, 2)

    start_x_line_row = START_POSITION_X + table_width - COLUMN_WIDTH_RANDOM[-1] + 10
    end_x_line_row = START_POSITION_X + table_width - 5
    xy_coordinates_line_row = (start_x_line_row, start_y_line_row, end_x_line_row, end_y_line_row)
    draw_line(draw, xy_coordinates_line_row, 2)

def label_ground_truth(PACKET_CONSTANT_LGT, PACKET_IMAGE_LGT, table_width, table_height, nums_rows, nums_columns, height_per_row):
    START_POSITION_X, START_POSITION_Y, COLUMN_WIDTH_RANDOM, TABLE_TYPE = PACKET_CONSTANT_LGT
    image_name, image_dir, image_width, image_height = PACKET_IMAGE_LGT

    # AUTOMATIC IMAGE LABELING
    root = ET.Element("annotation")
    ET.SubElement(root, "filename").text = image_name + '.jpg'
    ET.SubElement(root, "folder").text = FOLDER_SAVE_IMAGE_NAME
    ET.SubElement(root, "path").text = image_dir
    source = ET.SubElement(root, "source")
    ET.SubElement(source, "database").text = "Unknown"
    size = ET.SubElement(root, "size")
    ET.SubElement(size, "width").text = str(image_width)
    ET.SubElement(size, "height").text = str(image_height)
    ET.SubElement(size, "depth").text = "1"

    # Label class table
    bbox_xy_table = [START_POSITION_X, START_POSITION_Y, START_POSITION_X + table_width + 1, START_POSITION_Y + table_height + 1]
    automatic_label_class(root, 'table', bbox_xy_table)

    # Label class table row
    start_pos_class_row_x = START_POSITION_X
    end_pos_class_row_x = START_POSITION_X + table_width + 1
    cur_pos_class_row_y = START_POSITION_Y
    for i in range(nums_rows):
        if i != nums_rows - 1:
            bbox_xy_table_row = [start_pos_class_row_x, cur_pos_class_row_y, end_pos_class_row_x, cur_pos_class_row_y + height_per_row[i]]
        else:
            bbox_xy_table_row = [start_pos_class_row_x, cur_pos_class_row_y, end_pos_class_row_x, cur_pos_class_row_y + height_per_row[i] + 1]
        automatic_label_class(root, 'table row', bbox_xy_table_row)
        cur_pos_class_row_y += height_per_row[i]
        if i in TABLE_TYPE[14]:
            cur_pos_class_row_y += TABLE_TYPE[15]

    # Label class table column
    start_pos_class_column_y = START_POSITION_Y
    end_pos_class_column_y = START_POSITION_Y + table_height + 1
    cur_pos_class_column_x = START_POSITION_X
    for i in range(nums_columns):
        if i != nums_columns - 1:
            bbox_xy_table_column = [cur_pos_class_column_x, start_pos_class_column_y, cur_pos_class_column_x + COLUMN_WIDTH_RANDOM[i], end_pos_class_column_y]
        else:
            bbox_xy_table_column = [cur_pos_class_column_x, start_pos_class_column_y, cur_pos_class_column_x + COLUMN_WIDTH_RANDOM[i] + 1, end_pos_class_column_y]
        automatic_label_class(root, 'table column', bbox_xy_table_column)
        cur_pos_class_column_x += COLUMN_WIDTH_RANDOM[i]

    # Label class table column header
    if TABLE_TYPE[1] > 0:
        end_pos_class_column_header_y = START_POSITION_Y
        for i in range(TABLE_TYPE[1]):
            end_pos_class_column_header_y += height_per_row[i]
            if i in TABLE_TYPE[14] and i != TABLE_TYPE[1] - 1:
                end_pos_class_column_header_y += TABLE_TYPE[15]
        if TABLE_TYPE[1] == nums_rows:
            end_pos_class_column_header_y += 1
        bbox_xy_table_column_header = [START_POSITION_X, START_POSITION_Y, START_POSITION_X + table_width + 1, end_pos_class_column_header_y]
        automatic_label_class(root, 'table column header', bbox_xy_table_column_header)

    # Label class spanning cell
    if len(TABLE_TYPE[10]) > 0:
        for i in range(len(TABLE_TYPE[10])):
            start_cell = TABLE_TYPE[10][i][0]
            end_cell = TABLE_TYPE[10][i][-1]

            spc_width, spc_height, start_coordinates_spc_x, start_coordinates_spc_y = size_of_merged_cell(
                start_cell, end_cell, nums_columns, height_per_row, COLUMN_WIDTH_RANDOM, START_POSITION_X, START_POSITION_Y, TABLE_TYPE[14], TABLE_TYPE[15], True, nums_rows)

            bbox_xy_table_spanning_cell = [start_coordinates_spc_x, start_coordinates_spc_y, start_coordinates_spc_x + spc_width, start_coordinates_spc_y + spc_height]
            automatic_label_class(root, 'table spanning cell', bbox_xy_table_spanning_cell)

    # Label class projected row header
    if len(TABLE_TYPE[17]) > 0:
        for i in range(len(TABLE_TYPE[17])):
            start_cell = TABLE_TYPE[17][i][0]
            end_cell = TABLE_TYPE[17][i][-1]

            prh_width, prh_height, start_coordinates_prh_x, start_coordinates_prh_y = size_of_merged_cell(
                start_cell, end_cell, nums_columns, height_per_row, COLUMN_WIDTH_RANDOM, START_POSITION_X, START_POSITION_Y, TABLE_TYPE[14], TABLE_TYPE[15], True, nums_rows)

            bbox_xy_table_projected_row_header = [start_coordinates_prh_x, start_coordinates_prh_y, start_coordinates_prh_x + prh_width, start_coordinates_prh_y + prh_height]
            automatic_label_class(root, 'table projected row header', bbox_xy_table_projected_row_header)

    tree = ET.ElementTree(root)
    xml_dir = join_path(FOLDER_SAVE_IMAGE_DIR, image_name + '.xml')
    tree.write(xml_dir) 

def draw_table(data, img_idx, borderless, txt_dir):
    nums_rows, nums_columns, table_height, table_width, height_per_row, WoTIC, TABLE_TYPE, COLUMN_WIDTH_RANDOM = table_structure_range(data, txt_dir)

    PADDING_TABLE_X_LEFT, PADDING_TABLE_X_RIGHT = randint_padding_table(PADDING_TABLE_X)
    PADDING_TABLE_Y_TOP, PADDING_TABLE_Y_BOTTOM = randint_padding_table(PADDING_TABLE_Y)

    image_width = table_width + PADDING_TABLE_X_LEFT + PADDING_TABLE_X_RIGHT
    image_height = table_height + PADDING_TABLE_Y_TOP + PADDING_TABLE_Y_BOTTOM

    img = Image.new('RGB', (image_width, image_height), color = 'white')
    draw = ImageDraw.Draw(img)
    draw.fontmode = 'L'

    START_POSITION_X = PADDING_TABLE_X_LEFT
    START_POSITION_Y = PADDING_TABLE_Y_TOP

    cur_pos_rec_x = START_POSITION_X
    cur_pos_rec_y = START_POSITION_Y

    if not borderless:
        xy_coordinates_table = (cur_pos_rec_x, cur_pos_rec_y, cur_pos_rec_x + table_width, cur_pos_rec_y + table_height)
        draw.rectangle(xy_coordinates_table, outline='black', width=WIDTH_BORDER_LINE)

    height_font = HEIGHT_FONT
    height_line = HEIGHT_LINES
  
    with ThreadPool(processes=NUMS_SUB_PROCESSES) as sub_pool:    
        # Draw spanning cell
        PACKET_CONSTANT_DTPR = [START_POSITION_X, START_POSITION_Y, COLUMN_WIDTH_RANDOM, TABLE_TYPE]
        # draw_spanning_cell(PACKET_CONSTANT_DTPR, nums_columns, height_per_row, borderless, draw, height_line, height_font)
        sub_pool.apply_async(draw_spanning_cell, args=(PACKET_CONSTANT_DTPR, nums_columns, height_per_row, borderless, draw, height_line, height_font))

        # Draw table line
        PACKET_CONSTANT_DTL = [START_POSITION_X, START_POSITION_Y, COLUMN_WIDTH_RANDOM, TABLE_TYPE]
        # draw_table_line(PACKET_CONSTANT_DTL, table_width, height_per_row, draw, nums_rows)
        sub_pool.apply_async(draw_table_line, args=(PACKET_CONSTANT_DTL, table_width, height_per_row, draw, nums_rows))

        # VATI - Viet Anh Tools Images
        image_name = 'TABLE_VATI_' + CODEX_IMG_TYPE + '_' + CODEX_RC_TYPE + '_' + str(img_idx + 1)
        image_dir = join_path(FOLDER_SAVE_IMAGE_DIR, image_name + '.jpg')

        # Label ground truth
        PACKET_CONSTANT_LGT = [START_POSITION_X, START_POSITION_Y, COLUMN_WIDTH_RANDOM, TABLE_TYPE]
        PACKET_IMAGE_LGT = [image_name, image_dir, image_width, image_height]
        # label_ground_truth(PACKET_CONSTANT_LGT, PACKET_IMAGE_LGT, table_width, table_height, nums_rows, nums_columns, height_per_row)
        sub_pool.apply_async(label_ground_truth, args=(PACKET_CONSTANT_LGT, PACKET_IMAGE_LGT, table_width, table_height, nums_rows, nums_columns, height_per_row))

        # Draw table per row
        # Flatten 2D-Array SPC to 1D-Array SPC
        FLAT_TABLE_TYPE_10 = [spc_idx for spc in TABLE_TYPE[10] for spc_idx in spc]
        # CALCULATE 2 CONSTANTS ARRAY
        POS_REC_Y = []
        START_CELL_INDEX = []    
        HOON_BUFFER_PRY = START_POSITION_Y
        HOON_BUFFER_SCI = -1
        for idx in range(nums_rows):
            POS_REC_Y.append(HOON_BUFFER_PRY)
            HOON_BUFFER_PRY += height_per_row[idx]
            if idx in TABLE_TYPE[14]:
                HOON_BUFFER_PRY += TABLE_TYPE[15]
            START_CELL_INDEX.append(HOON_BUFFER_SCI)
            HOON_BUFFER_SCI += nums_columns
        PACKET_CONSTANT = [START_POSITION_X, TABLE_TYPE, COLUMN_WIDTH_RANDOM, FLAT_TABLE_TYPE_10, POS_REC_Y, START_CELL_INDEX]
        # draw_table_per_row(data, PACKET_CONSTANT, WoTIC, height_per_row, borderless, draw, height_line, height_font)
        sub_pool.apply_async(draw_table_per_row, args=(data, PACKET_CONSTANT, WoTIC, height_per_row, borderless, draw, height_line, height_font))

        sub_pool.close()
        sub_pool.join()

    # Save gen image
    img.save(image_dir)

    # Log finalize
    print('Successfully created image and ground truth {:03}'.format(img_idx + 1))

def create_image(csv, img_idx):
    data = read_csv(csv)
    txt = csv.replace('.csv', '.txt')
    draw_table(data, img_idx, BORDERLESS, txt)

def main():
    # START
    print('Program start !!!')
    start_time_program = time.time()
    
    # Create or Refresh output folder
    make_dirs_or_format_dir(FOLDER_SAVE_IMAGE_DIR)

    # Get file list (.csv & .txt)
    csv_list = get_sample_list_from_path(CSV_FOLDER_DIR, 'csv')
    csv_list.sort(key=natsort_key)

    # Create image and ground truth from csv file
    with ThreadPool(processes=NUMS_PROCESSES) as pool:
        img_idx = -1
        for csv in csv_list:
            img_idx += 1
            pool.apply_async(create_image, args=(csv, img_idx))
        pool.close()
        pool.join()

    end_time_program = time.time()
    total_time_program = end_time_program - start_time_program
    mean_times_per_img = total_time_program / len(csv_list)
    mean_img_and_grt_per_sec = 1 / mean_times_per_img
    print('Average times per image:', round(mean_times_per_img, 5), '(s)')
    print('Average image create per second: {:.3f}'.format(round(mean_img_and_grt_per_sec, 3)), '(img/s)')
    print('Total running time:', round(total_time_program, 5), '(s)')
    
    # END
    print('Program run successfully !!!')
    print("Copyright @ProjectHoon")

if __name__ == '__main__':
    main()
