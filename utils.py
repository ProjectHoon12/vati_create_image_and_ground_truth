import os
import csv
import random
import shutil
import xml.etree.ElementTree as ET

from glob import glob
from PIL import Image, ImageDraw, ImageFont
from dict_table_type import DICT_TABLE_TYPE
from list_table_type import LIST_TABLE_TYPE
from unidecode import unidecode


def join_path(base_path, relative_path):
    return os.path.join(base_path, relative_path)

def format_folder(path):
    if os.path.exists(path):
        shutil.rmtree(path)
    os.mkdir(path)

def make_dirs_or_format_dir(folder_path):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    else:
        format_folder(folder_path)

def read_csv(filename):
    with open(filename, encoding='utf-8-sig') as f:
        reader = csv.reader(f)
        data = list(reader)
    return data

def get_width_of_some_tokens(tokens, font):
    unidecode_tokens = unidecode(tokens)
    width = font.getlength(unidecode_tokens)
    return width

def get_height_of_some_tokens(tokens, font):
    unidecode_tokens = unidecode(tokens)
    height = font.getsize(unidecode_tokens)[1]
    return height

def my_textwrap_tokens(tokens, base_width, font):
    split_tokens = tokens.split()
    line = ''
    pre_line = ''
    lines = []
    add_line_flag = False
    i = 0

    while i < len(split_tokens):
        if not add_line_flag:
            line += split_tokens[i]
        else:
            i -= 1
        width_line = get_width_of_some_tokens(line, font)
        if width_line > base_width:
            lines.append(pre_line)
            line = line.replace(pre_line, '')
            line = line.lstrip()
            pre_line = ''
            add_line_flag = not add_line_flag
        else:
            pre_line = line
            line += ' '
            add_line_flag = False
        i += 1
    if not line == '':
        if line[0] == ' ':
            line = line.lstrip()
        lines.append(line)
    return lines

def randint_differrence_range(start_range, end_range):
    return random.randint(start_range, end_range)

def randint_with_probability(start_range, end_range, mid_range, probability):
    if random.random() < probability:
        return randint_differrence_range(start_range, mid_range)
    else:
        return randint_differrence_range(mid_range, end_range)

def randint_padding_table(padding_table):
    mid_range = 69
    probability = 0.08
    padding_table_1 = randint_with_probability(padding_table[0], padding_table[1], mid_range, probability)
    padding_table_2 = randint_with_probability(padding_table[0], padding_table[1], mid_range, probability)
    return padding_table_1, padding_table_2

def get_sample_list_from_path(path, sample_format):
    if os.path.exists(path):
        return glob(join_path(path, f'*{sample_format}'))

def automatic_label_class(root, class_name, xy_bbox):
    object_class = ET.SubElement(root, "object")
    ET.SubElement(object_class, "name").text = class_name
    ET.SubElement(object_class, "pose").text = "Unspecified"
    ET.SubElement(object_class, "truncated").text = "0"
    ET.SubElement(object_class, "difficult").text = "0"
    bndbox_class = ET.SubElement(object_class, "bndbox")
    ET.SubElement(bndbox_class, "xmin").text = str(xy_bbox[0])
    ET.SubElement(bndbox_class, "ymin").text = str(xy_bbox[1])
    ET.SubElement(bndbox_class, "xmax").text = str(xy_bbox[2])
    ET.SubElement(bndbox_class, "ymax").text = str(xy_bbox[3])

def get_margin_space_x(horizontal_string, cell_width, tokens, cell_font_cur, margin_space):
    if horizontal_string == 'Left':
        return margin_space[0]
    elif horizontal_string == 'Center':
        return (cell_width - get_width_of_some_tokens(tokens, cell_font_cur)) / 2
    elif horizontal_string == 'Right':
        return cell_width - (get_width_of_some_tokens(tokens, cell_font_cur) + margin_space[2])

def get_margin_space_y(vertical_string, cell_height, nums_tokens, height_line, height_font, margin_space, default_space_per_row):
    if vertical_string == 'Top':
        return margin_space[1]
    elif vertical_string == 'Middle':
        return (cell_height - (nums_tokens * height_line + (nums_tokens - 1) * default_space_per_row)) / 2
    elif vertical_string == 'Bottom':
        return cell_height - ((nums_tokens - 1) * (height_line + default_space_per_row) + height_font + margin_space[3])

def identify_merged_cell(start_cell, end_cell, nums_columns):
    row_start = start_cell // nums_columns
    column_start = start_cell % nums_columns
    row_end = end_cell // nums_columns
    column_end = end_cell % nums_columns
    return row_start, column_start, row_end, column_end

def size_of_merged_cell(start_cell, end_cell, nums_columns, height_per_row, COLUMN_WIDTH_RANDOM, 
                        start_coordinates_merged_cell_x, start_coordinates_merged_cell_y, SPACE_ROWS, SPACE_BLOCK, ground_truth=False, nums_rows=0):
    row_start, column_start, row_end, column_end = identify_merged_cell(start_cell, end_cell, nums_columns)

    merged_cell_width = 0
    merged_cell_height = 0

    for idx in range(row_start):
        start_coordinates_merged_cell_y += height_per_row[idx]
        if idx in SPACE_ROWS:
            start_coordinates_merged_cell_y += SPACE_BLOCK
    for idx in range(row_start, row_end + 1, 1):
        merged_cell_height += height_per_row[idx]
        if idx in SPACE_ROWS and idx != row_end:
            merged_cell_height += SPACE_BLOCK
    for idx in range(column_start):
        start_coordinates_merged_cell_x += COLUMN_WIDTH_RANDOM[idx]
    for idx in range(column_start, column_end + 1, 1):
        merged_cell_width += COLUMN_WIDTH_RANDOM[idx]

    if ground_truth:
        if row_end == nums_rows - 1:
            merged_cell_height += 1
        if column_end == nums_columns - 1:
            merged_cell_width += 1

    return merged_cell_width, merged_cell_height, start_coordinates_merged_cell_x, start_coordinates_merged_cell_y

def draw_line(draw, xy_coordinates_line, width):
    draw.line(xy_coordinates_line, fill='black', width=width)
