# # TABLE TYPE BHXH3 - 3.1.1.1
# BHXH3_3111 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     True,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     3,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     4,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     ['STT', 
#      'STT theo TT 43', 
#      'DANH MỤC KỸ THUẬT', 
#      'PHÂN TUYẾN KỸ THUẬT'],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [['STT'],
#      ['STT', 'theo', 'TT 43'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['PHÂN TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     7,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Left', 'Top'],
#      ['Left', 'Top'],
#      ['Left', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [2, 14, 2, 14],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [74, 168, 624, 58, 58, 58, 58],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [[3, 4, 5, 6],
#      [10, 11, 12, 13]],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [['PHÂN TUYẾN', 'KỸ THUẬT'],
#      ['4']],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     [['Center', 'Middle'],
#      ['Center', 'Middle']]
# ]

# # TABLE TYPE BHXH3 - 3.1.2.1
# BHXH3_3121 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     True,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     2,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     4,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     ['STT của Đvị', 
#      'STT theo TT 43/2013', 
#      'DANH MỤC KỸ THUẬT', 
#      'PHÂN TUYẾN KỸ THUẬT'],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [['STT', 'của', 'Đvị'],
#      ['STT theo', 'TT 43/2013'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['PHÂN TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     7,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Left', 'Top'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [12, 2, 12, 2],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [74, 168, 624, 58, 58, 58, 58],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [[3, 4, 5, 6]],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [['PHÂN TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     [['Center', 'Middle']]
# ]

# # TABLE TYPE BHXH3 - 3.1.2.2
# BHXH3_3122 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     True,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     2,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     4,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     ['TT', 
#      'TT TT43', 
#      'DANH MỤC KỸ THUẬT', 
#      'PHÂN TUYẾN KỸ THUẬT'],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [['TT'],
#      ['TT', 'TT43'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['PHÂN TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     7,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [13, 3, 13, 3],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [66, 88, 564, 78, 78, 78, 78],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [[0, 7],
#      [1, 8],
#      [2, 9],
#      [3, 4, 5, 6]],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [['TT'],
#      ['TT', 'TT43'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['PHÂN TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']]
# ]

# # TABLE TYPE BHXH3 - 3.1.3.1
# BHXH3_3131 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     True,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     3,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     3,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     ['TT',
#      'DANH MỤC KỸ THUẬT', 
#      'PHÂN TUYẾN KỸ THUẬT'],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [['TT'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['PHÂN TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     6,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Top'],
#      ['Left', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [12, 2, 12, 2],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [78, 618, 62, 62, 62, 62],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [[2, 3, 4, 5],
#      [6, 12],
#      [7, 13],
#      [8, 9, 10, 11]],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [['PHÂN TUYẾN', 'KỸ THUẬT'],
#      ['1'],
#      ['2'],
#      ['3']],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']]
# ]

# # TABLE TYPE BHXH3 - 3.1.K.1
# BHXH3_31K1 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     True,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     2,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     5,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     ['STT',
#      'STT/TT 43/2013',
#      'STT/TT 21/2017',
#      'DANH MỤC KỸ THUẬT', 
#      'PHÂN TUYẾN KỸ THUẬT'],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [['S', 'TT'],
#      ['STT/TT', '43/2013'],
#      ['STT/TT', '21/2017'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['PHÂN', 'TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     8,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [12, 2, 12, 2],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [76, 120, 120, 610, 60, 60, 60, 60],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [[4, 5, 6, 7]],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [['PHÂN', 'TUYẾN', 'KỸ THUẬT']],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     [['Center', 'Middle']]
# ]

# # TABLE TYPE BHXH3 - 3.2.1.1
# BHXH3_3211 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     True,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     1,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     5,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     ['TT',
#      'Mã số',
#      'DANH MỤC KỸ THUẬT',
#      'Tuyến', 
#      'Phân loại'],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [['TT'],
#      ['Mã số'],
#      ['DANH MỤC KỸ THUẬT'],
#      ['Tuyến'],
#      ['Phân', 'loại']],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     5,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Middle'],
#      ['Center', 'Top'],
#      ['Left', 'Middle'],
#      ['Center', 'Top'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [14, 3, 14, 3],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [68, 118, 634, 106, 98],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # TABLE TYPE BHXH3 - 3.ND.NH.3111
# BHXH3_3NDNH_3111 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     False,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     0,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     0,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     [],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     7,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Top'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [14, 2, 14, 22],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [72, 170, 626, 60, 60, 60, 60],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # TABLE TYPE BHXH3 - 3.ND.NH.3121
# BHXH3_3NDNH_3121 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     False,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     0,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     0,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     [],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     7,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Left', 'Top'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [12, 2, 12, 2],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [74, 168, 624, 58, 58, 58, 58],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # TABLE TYPE BHXH3 - 3.ND.NH.3122
# BHXH3_3NDNH_3122 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     False,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     0,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     0,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     [],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     7,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Left', 'Middle'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Top'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [14, 3, 14, 3],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [70, 84, 580, 82, 82, 82, 82],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # TABLE TYPE BHXH3 - 3.ND.NH.3131
# BHXH3_3NDNH_3131 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     False,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     0,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     0,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     [],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     6,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Top'],
#      ['Left', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top'],
#      ['Center', 'Top']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [5, 13, 5, 13],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [78, 618, 62, 62, 62, 62],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # TABLE TYPE BHXH3 - 3.ND.NH.31K1
# BHXH3_3NDNH_31K1 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     False,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     0,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     0,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     [],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     8,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [12, 2, 12, 2],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [76, 120, 120, 610, 60, 60, 60, 60],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # TABLE TYPE BHXH3 - 3.ND.NH.3211
# BHXH3_3NDNH_3211 = [
#     # PROPERTIES 00 (TYPE: BOOL)
#     # Have HEADER
#     False,
    
#     # PROPERTIES 01 (TYPE: INT)
#     # Nums rows of HEADER
#     0,
    
#     # PROPERTIES 02 (TYPE: INT)
#     # Nums cells in first row of HEADER
#     0,
    
#     # PROPERTIES 03 (TYPE: STRING)
#     # Content per cell in first row of HEADER
#     [],

#     # PROPERTIES 04 (TYPE: STRING)
#     # Split content per cell in first row of HEADER
#     [],

#     # PROPERTIES 05 (TYPE: STRING)
#     # Alignment of text per cell in first row of HEADER
#     # ['Horizontal', 'Vertical']
#     # Horizontal: Left - Center - Right
#     # Vertical: Top - Middle - Bottom
#     [],
    
#     # PROPERTIES 06 (TYPE: INT)
#     # Nums columns of table
#     5,

#     # PROPERTIES 07 (TYPE: STRING)
#     # Alignment of text per column (not include first row of HEADER)
#     [['Center', 'Middle'],
#      ['Center', 'Top'],
#      ['Left', 'Middle'],
#      ['Left', 'Top'],
#      ['Center', 'Middle']],

#     # PROPERTIES 08 (TYPE: INT)
#     # Margin space
#     # ['Left', 'Top', 'Right', 'Bottom']
#     [4, 15, 4, 15],

#     # PROPERTIES 09 (TYPE: INT)
#     # Column width list default
#     [70, 120, 636, 110, 100],

#     # PROPERTIES 10 (TYPE: INT)
#     # Cell INDEX of spanning cell
#     # []
#     [],

#     # PROPERTIES 11 (TYPE: STRING)
#     # Content of spanning cell
#     [],

#     # PROPERTIES 12 (TYPE: STRING)
#     # Alignment of text in spanning cell
#     []
# ]

# # LIST TABLE TYPE
# LIST_TABLE_TYPE = [
#     BHXH3_3111,
#     BHXH3_3121,
#     BHXH3_3122,
#     BHXH3_3131,
#     BHXH3_31K1,
#     BHXH3_3211,
#     BHXH3_3NDNH_3111,
#     BHXH3_3NDNH_3121,
#     BHXH3_3NDNH_3122,
#     BHXH3_3NDNH_3131,
#     BHXH3_3NDNH_31K1,
#     BHXH3_3NDNH_3211
# ]

# TABLE TYPE BCTC - 3.CDKT.A.1
BCTC3_CDKT_A_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Tại ngày'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Tại ngày']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Top'],
     ['Left', 'Top'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [10, 4, 10, 4],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [66, 760, 116, 168, 168],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Tại ngày']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Bottom'],
     ['Center', 'Bottom']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [3, 4, 5, 6, 9, 12, 13, 16, 20, 26, 33, 39],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2, 3, 4, 5, 8, 11, 12, 15, 19, 25, 29, 32, 38],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    25,
    
    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    5,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    [[15, 16, 17, 18, 19]]
]

# TABLE TYPE BCTC - 3.CDKT.B.1
BCTC3_CDKT_B_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Tại ngày'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Tại ngày']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Top'],
     ['Left', 'Top'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [76, 600, 116, 188, 188],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Tại ngày']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Bottom'],
     ['Center', 'Bottom']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [3, 4, 7, 8, 9, 10, 13, 14, 22, 23],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2, 3, 6, 7, 8, 9, 12, 13, 21, 22, 23],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    35,

    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    [[15, 16, 17, 18, 19]]
]

# TABLE TYPE BCTC - 3.CDKT.C.1
BCTC3_CDKT_C_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Tại ngày'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Tại ngày']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Top'],
     ['Left', 'Top'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [76, 565, 112, 195, 205],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Tại ngày']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Middle'],
     ['Center', 'Middle']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    30,
    
    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    []
]

# TABLE TYPE BCTC - 3.LCTT.A.1
BCTC3_LCTT_A_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Cho năm tài chính kết thúc ngày 31 tháng 12 năm'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Top'],
     ['Left', 'Top'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [76, 746, 90, 165, 165],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Bottom'],
     ['Center', 'Bottom']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [3, 12, 13, 20, 29],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2, 11, 12, 19, 28],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    40,
    
    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    [[15, 16, 17, 18, 19]],

    # PROPERTIES 18 (TYPE: INT)
    # Draw line rows
    []
]

# TABLE TYPE BCTC - 3.LCTT.B.1
BCTC3_LCTT_B_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Cho năm tài chính kết thúc ngày 31 tháng 12 năm'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Top'],
     ['Left', 'Top'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [65, 660, 115, 165, 165],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Bottom'],
     ['Center', 'Bottom']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [3, 8, 9, 13, 14, 15, 16],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2, 7, 8, 12, 13, 14, 15],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    32,
    
    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,
    
    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    []
]

# TABLE TYPE BCTC - 3.KQKD.A.1
BCTC3_KQKD_A_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Cho năm tài chính kết thúc ngày 31 tháng 12 năm'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Bottom'],
     ['Left', 'Bottom'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [55, 600, 115, 165, 165],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Bottom'],
     ['Center', 'Bottom']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [5, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 22, 23],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [3, 5, 6, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 22, 23],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    30,
    
    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    []
]

# TABLE TYPE BCTC - 3.KQKD.B.1
BCTC3_KQKD_B_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    4,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['', 
     '', 
     'Thuyết minh', 
     'Cho năm tài chính kết thúc ngày 31 tháng 12 năm'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     [''],
     ['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    5,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Left', 'Top'],
     ['Left', 'Top'],
     ['Center', 'Bottom'],
     ['Right', 'Bottom'],
     ['Right', 'Bottom']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [65, 660, 115, 165, 165],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[2, 7, 12],
     [3, 4]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
    [['Thuyết', 'minh'],
     ['Cho năm tài chính kết thúc', 'ngày 31 tháng 12 năm']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
    [['Center', 'Middle'],
     ['Center', 'Middle']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [3, 8, 9, 13, 14, 15, 16],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2, 7, 8, 12, 13, 14, 15],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    32,

    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    []
]

# TABLE TYPE BCTC - 3.KHAC.A.1
BCTC3_KHAC_A_1 = [
    # PROPERTIES 00 (TYPE: BOOL)
    # Have HEADER
    True,
    
    # PROPERTIES 01 (TYPE: INT)
    # Nums rows of HEADER
    3,
    
    # PROPERTIES 02 (TYPE: INT)
    # Nums cells in first row of HEADER
    3,
    
    # PROPERTIES 03 (TYPE: STRING)
    # Content per cell in first row of HEADER
    ['',  
     'Tăng/(Giảm) điểm cơ bản', 
     'Ảnh hưởng đến'],

    # PROPERTIES 04 (TYPE: STRING)
    # Split content per cell in first row of HEADER
    [[''],
     ['Tăng/(Giảm) điểm', 'cơ bản'],
     ['Ảnh hưởng đến']],

    # PROPERTIES 05 (TYPE: STRING)
    # Alignment of text per cell in first row of HEADER
    # ['Horizontal', 'Vertical']
    # Horizontal: Left - Center - Right
    # Vertical: Top - Middle - Bottom
    [['Center', 'Middle'],
     ['Right', 'Middle'],
     ['Center', 'Middle']],
    
    # PROPERTIES 06 (TYPE: INT)
    # Nums columns of table
    4,

    # PROPERTIES 07 (TYPE: STRING)
    # Alignment of text per column (not include first row of HEADER)
    [['Center', 'Top'],
     ['Right', 'Top'],
     ['Right', 'Top'],
     ['Right', 'Top']],

    # PROPERTIES 08 (TYPE: INT)
    # Margin space
    # ['Left', 'Top', 'Right', 'Bottom']
    [12, 2, 2, 2],

    # PROPERTIES 09 (TYPE: INT)
    # Column width list default
    [74, 294, 294, 294],

    # PROPERTIES 10 (TYPE: INT)
    # Cell INDEX of spanning cell
    # []
    [[1, 5, 9],
     [2, 3]],

    # PROPERTIES 11 (TYPE: STRING)
    # Content of spanning cell
     [['Tăng/(Giảm) điểm', 'cơ bản'],
     ['Ảnh hưởng đến']],

    # PROPERTIES 12 (TYPE: STRING)
    # Alignment of text in spanning cell
     [['Right', 'Middle'],
     ['Center', 'Middle']],

    # PROPERTIES 13 (TYPE: INT)
    # Bold rows
    [],

    # PROPERTIES 14 (TYPE: INT)
    # Space rows
    [2, 4],

    # PROPERTIES 15 (TYPE: INT)
    # Big spacing betwwen two blocks
    30,

    # PROPERTIES 16 (TYPE: INT)
    # Default spacing between two rows
    2,

    # PROPERTIES 17 (TYPE: INT)
    # Cell INDEX of projected row header
    # []
    []    
]

# DICT TABLE TYPE BCTC
DICT_TABLE_TYPE = {
    'BCTC3_CDKT_A_1': BCTC3_CDKT_A_1,
    'BCTC3_CDKT_B_1': BCTC3_CDKT_B_1,
    'BCTC3_CDKT_C_1': BCTC3_CDKT_C_1,
    'BCTC3_LCTT_A_1': BCTC3_LCTT_A_1,
    'BCTC3_LCTT_B_1': BCTC3_LCTT_B_1,
    'BCTC3_KQKD_A_1': BCTC3_KQKD_A_1,
    'BCTC3_KQKD_B_1': BCTC3_KQKD_B_1,
    'BCTC3_KHAC_A_1': BCTC3_KHAC_A_1
}
